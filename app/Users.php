<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    public function roles()
    {
      return $this->belongsTo('App\Roles');
    }

    protected $primarykey = ['id'];
    protected $fillable = ['name'];
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            if(empty($model->{$model->getKeyName()}))
            {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

}
