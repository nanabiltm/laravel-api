<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table>
        <thead>
         <tr>
          <th>Roles</th>
          <th>Name</th>
          <th>Username</th>
          <th>Email</th>
         </tr>
        <tbody>
         @foreach($roles as $value)
         <tr> 
          <td>{{$value->role_id}}</td>
          <td>{{$value->name}}</td>
          <td>{{$value->username}}</td>
          <td>{{$value->email}}</td>
         
          <td>
             @foreach($value->users as $b)
             {{$b->nama}}
             @endforeach
          </td>
         </tr>
         @endforeach
        <tbody>
        </thead>
      </table>
</body>
</html>